import { NgModule } from '@angular/core';
import { BdcComponent } from './bdc.component';



@NgModule({
  declarations: [
    BdcComponent
  ],
  imports: [
  ],
  exports: [
    BdcComponent
  ]
})
export class BdcModule { }
